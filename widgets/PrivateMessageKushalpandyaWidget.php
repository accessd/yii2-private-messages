<?php

/**
 * Created by PhpStorm.
 * User: VisioN
 * Date: 04.06.2015
 * Time: 12:57
 */

namespace vision\messages\widgets;

use common\models\User;
use vision\messages\assets\MessageKushalpandyaAssets;


class PrivateMessageKushalpandyaWidget extends PrivateMessageWidget
{

    public function run()
    {
        $this->assetJS();
        $this->addJs();
        $this->html = '<div id="messag_4NrS3" class="main-message-container">';
        $this->html .= '<div class="message-north">';
        $this->html .= $this->getListUsers();
        $this->html .= $this->getBoxMessages();
        $this->html .= '</div>';
        $this->html .= $this->getFormInput();
        $this->html .= '</div>';
        return $this->html;
    }


    protected function getListUsers()
    {
        $users = \Yii::$app->mymessages->getAllUsers();
        $html = '<ul class="list_users message-user-list">';

        foreach ($users as $usr) {
            $userDepartment = \common\models\UserDepartment::find()->joinWith('user')->where(['user.id' => $usr['id']])->one();
            $user = User::findOne($usr['id'])->getFullName();
            $html .= '<li class="contact" data-user="' . $usr['id'] . '"><a href="#">';

            $html .= '<span>' .  $user;
            if (isset($userDepartment->department)) {
                $html .= '<p style="font-size: 10px;">' . $userDepartment->department->name . '</p>';
            }
            if (!empty($usr['cnt_mess'])) {
                $html .= ' <span id="cnt" class="label label-success">';
                if ($usr['cnt_mess']) {
                    $html .=  $usr['cnt_mess'];
                } else
                    $html .= "</span>";
            }
            $html .= "</span></a></li>";
        }
        $html .= '</ul>';
        return $html;
    }


    protected function getBoxMessages()
    {
        $html = '';
        $html .= '<div class="message-container message-thread">';
        $html .= '</div>';
        return $html;
    }


    protected function getFormInput()
    {
        $html = '<div class="message-south"><form action="#" class="message-form" method="POST">';
        $html .= '<textarea disabled="true" name="input_message"></textarea>';
        $html .= '<input type="hidden" name="message_id_user" value="">';
        $html .= '<button type="submit" class="btn btn-info">' . $this->buttonName . '</button>';
        $html .= \Yii::$app->mymessages->enableEmail ? '<span class="send_mail"><input class="checkbox" id="send_mail" type="checkbox" name="send_mail" value="1"><label for="send_mail">Отправить также на email</label></span>' : '';
        $html .= '</form></div>';
        return $html;
    }


    protected function assetJS()
    {
        MessageKushalpandyaAssets::register($this->view);
    }
}
